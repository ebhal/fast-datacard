=======
History
=======
0.1.13 (2020-09-25)
------------------

* Added possibility of having ad-hoc appendix parameters

0.1.12 (2020-09-09)
------------------

* Customizable threshold for autoMCStats threshold

0.1.11 (2020-09-08)
------------------

* Added option to define region-specific signals -- just like backgrounds

0.1.10 (2020-08-10)
------------------

* Remove bug that affected the observed data yield in the signal region

0.1.9 (2020-07-31)
------------------

* Remove bug that affected the observed data yield in the signal region

0.1.8 (2020-06-09)
------------------

* Make sure the dataframe is read according to the correct dataframe separator (comma or tab)

0.1.7 (2020-06-04)
------------------

* Adding minor features like adding categories names at the end of processes names, and appendix (in dev)
* Change ways the environment is setup
* Minor bug fixes

0.1.6 (2020-05-18)
------------------

* Add error to histograms

0.1.5 (2018-04-29)
------------------

* Fix typo

0.1.4 (2018-04-29)
------------------

* Added error message to explain crash

0.1.3 (2018-04-05)
------------------

* Easier handling of dataframe files

0.1.2 (2018-04-04)
------------------

* Updated executable name and documentation

0.1.1 (2018-10-01)
------------------

* added initial documentation

0.1.0 (2018-08-21)
------------------

* First release on PyPI.
